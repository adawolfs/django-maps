from django import template

register = template.Library()

@register.filter
def lastAttend(student, xs):
	return student.attend_set.filter(_class = xs).first().date