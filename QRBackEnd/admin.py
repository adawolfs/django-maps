from django.contrib import admin

from .models import User, Student, Class, Attend

admin.site.register(User)
admin.site.register(Student)
admin.site.register(Class)
admin.site.register(Attend)


# Register your models here.
