from django.db import models

# Create your models here.
class User(models.Model):
	full_name = models.CharField(max_length=200)
	name = models.CharField(max_length=100)
	password = models.CharField(max_length=200)
	email = models.EmailField()

class Student(models.Model):
	full_name = models.CharField(max_length=200)
	name = models.CharField(max_length=100)
	password = models.CharField(max_length=200)
	def __str__(self):         
		return '%s' % (self.full_name)

class Class(models.Model):
	name = models.CharField(max_length=200)
	cod = models.CharField(max_length=200)
	students = models.ManyToManyField(Student)
	def __str__(self):         
		return '%s : %s' % (self.cod, self.name)

class Attend(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	latitude = models.CharField(max_length=100, default="")
	longitude = models.CharField(max_length=100, default="")
	st = models.ForeignKey(Student, on_delete=models.CASCADE)
	cl =  models.ForeignKey(Class, on_delete=models.CASCADE)

	def __str__(self):         
		return '%s - %s - %s - %s' % (self.id, self.st.full_name, self.cl.name, self.date)