from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^map/(?P<attendId>[0-9]*)/$', views.map, name='map'),
    url(r'^loginApp$', views.loginApp, name='loginApp'),
    url(r'^registerQR$', views.registerQR, name='registerQR'),
]
