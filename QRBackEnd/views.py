from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from .models import Student
from .models import Class
from .models import Attend

import json, html

def index(request):
	attend_list = Attend.objects.all()
	class_list = Class.objects.all()
	template = loader.get_template('index.html')
	context = {
		'attend_list': attend_list,
		'class_list': class_list,
	}
	return HttpResponse(template.render(context, request))

def map(request, attendId):
	a = Attend.objects.get(id=attendId)
	template = loader.get_template('map.html')
	context = {
		'attend': a
	}
	return HttpResponse(template.render(context, request))

@csrf_exempt
def loginApp(request):
	n = request.POST.get('name','')
	p = request.POST.get('password','')
	st = Student.objects.filter(name=n, password = p).first()
	print(st)
	data = {}
	if st:
		data['userId'] = st.id
		data['success'] = True
	else:
		data['success'] = False
	return JsonResponse(data)

#{"class":"progra1","latitude":"1232","longitude":"213"}
@csrf_exempt
def registerQR(request):
	qr = html.unescape(request.POST.get('qr',''))
	lat = html.unescape(request.POST.get('lat',''))
	lon = html.unescape(request.POST.get('long',''))

	userId = request.POST.get('userId','')
	print(qr)
	print(userId)
	cl = Class.objects.filter(cod=qr).first()
	st = Student.objects.get(pk=userId)
	data = {}
	if st and cl:
		a = Attend(cl=cl, st=st, latitude=lat, longitude=lon)
		a.save()
		data['success'] = True
	else:
		data['success'] = False
	return JsonResponse(data) 
