from django.apps import AppConfig


class QrbackendConfig(AppConfig):
    name = 'QRBackEnd'
